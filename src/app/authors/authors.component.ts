import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  typesOfAuthors: string[] = ['Lewis Carrol', 'Leo Tolstoy', 'Loafers', 'Thomas Mann', 'Venta'];
  constructor() { }

  ngOnInit() {
  }

}
